package Student;

import java.util.Arrays;
import java.util.Objects;

public class Student {
    private String surname;
    private String name;
    private int groupNumber;
    private int[] grades;

    public Student(String surname, String name, int groupNumber, int[] grades) {
        this.surname = surname;
        this.name = name;
        this.groupNumber = groupNumber;
        this.grades = grades;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Student student = (Student) o;

        if (groupNumber != student.groupNumber) {
            return false;
        }
        if (!Objects.equals(surname, student.surname)) {
            return false;
        }
        if (!Objects.equals(name, student.name)) {
            return false;
        }
        return Arrays.equals(grades, student.grades);
    }

    @Override
    public int hashCode() {
        int result = surname != null ? surname.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + groupNumber;
        result = 31 * result + Arrays.hashCode(grades);
        return result;
    }

    @Override
    public String toString() {
        return "Student.Student{" +
                "surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", groupNumber=" + groupNumber +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }
}
