package Student;

import Student.Student;

public class StudentRepository {
    public Student[] students;
    public StudentRepository(Student[] students){
        this.students = students;
    }
    private Student[] extractSubArray(Student[] source, int count){
        Student[] destination = new Student[count];
        System.arraycopy(source,0,destination,0,count);
        return destination;
    }
    public Student[] findStudentsByGradesMoreThan(int grade){
        Student[] result = new Student[students.length];
        int count=0;
        for (int i=0;i<students.length;i++){
            Student student = students[i];
            boolean gradesMoreThanNeeded = false;
            if( student!=null && student.getGrades()!=null){
                for (int gradeOfStudent : student.getGrades()) {
                    if(gradeOfStudent<grade){
                        gradesMoreThanNeeded=false;
                    }else{
                        gradesMoreThanNeeded=true;
                    }
                }
                if(gradesMoreThanNeeded){
                    result[count++]=student;
                }

            }
        }
        Student[] destination = extractSubArray(result,count);

        return destination;
    }

}
