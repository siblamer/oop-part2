package Train;

public class TrainPrinter {
    public void print(Train[] trains){
        for (Train train: trains) {
            print(train);
        }
    }
    public void print(Train train){
        System.out.println(" "+train.toString());
    }
}
