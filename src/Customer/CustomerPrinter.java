package Customer;

public class CustomerPrinter {
    public void print(Customer[] customers){
        for (Customer customer: customers) {
            print(customer);
        }
    }
    public void print(Customer customer){
        System.out.println(" "+customer.toString());
    }
}
