package Customer;

import java.util.Arrays;

public class CustomerDatabase {

    private Customer[] customers;

    public CustomerDatabase(Customer[] customers) {
        this.customers = customers;
    }
    private Customer[] extractSubArray(Customer[] source, int count){
        Customer[] destination = new Customer[count];
        System.arraycopy(source,0,destination,0,count);
        return destination;
    }
    public Customer[] sortedByFullname(){
        Customer[] result = customers;

        int len = result.length;
        for (int i=0;i<len-1;++i){
            for(int j=0;j<len-i-1; ++j) {
                Customer customer1 = result[j+1];
                Customer customer2 = result[j];
                String customer1Fullname = customer1.getFullname();
                String customer2Fullname = customer2.getFullname();
                if(result!=null && customer1Fullname.compareTo(customer2Fullname) > 0){
                    Customer swap = result[j];
                    result[j] = result[j+1];
                    result[j+1] = swap;
                }
            }
        }
        return result;
    }

    public Customer[] filterByCardDiaposon(long a, long b ){
        Customer[] result = customers;
        int count=0;
        for(Customer customer: customers){
            if(customer.getCardnumber() > a && customer.getCardnumber()<b){
                result[count++]=customer;
            }
        }

        Customer[] destination = extractSubArray(result,count);
        return destination;
    }

    @Override
    public String toString() {
        return "Customer.CustomerDatabase{" +
                "customers=" + Arrays.toString(customers) +
                '}';
    }
}
